class Pull
  EMOJI = ENV["MERGE_REQUEST_EMOJI"]

  def initialize
    @draft = Draft.new
  end

  def call
    merge_requests.each do |merge_request|
      draft.update(merge_request)
      delete_emoji(merge_request)
    end
  end

  private

  attr_reader :draft

  def merge_requests
    Gitlab.user_merge_requests(
      state: :merged,
      scope: :all,
      project: "gitlab-org/gitlab",
      my_reaction_emoji: EMOJI,
    )
  end

  def delete_emoji(merge_request)
    Gitlab.delete_award_emoji(merge_request.project_id, merge_request.iid, "merge_request", my_emoji(merge_request).id)
  end

  def my_emoji(merge_request)
    Gitlab.award_emojis(merge_request.project_id, merge_request.iid, "merge_request").each do |emoji|
      return emoji if emoji.name == EMOJI && emoji.user.id == user_id
    end
  end

  def user_id
    @user_id ||= Gitlab.user.id
  end
end
