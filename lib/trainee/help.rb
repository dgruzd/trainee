class Help
  def call
    <<~HELP

                       *.                  *.
                      ***                 ***
                     *****               *****
                    .******             *******
                    ********            ********
                   ,,,,,,,,,***********,,,,,,,,,
                  ,,,,,,,,,,,*********,,,,,,,,,,,
                  .,,,,,,,,,,,*******,,,,,,,,,,,,
                      ,,,,,,,,,*****,,,,,,,,,.
                         ,,,,,,,****,,,,,,
                            .,,,***,,,,
                                ,*,.

      Trainee
       ̅ ̅ ̅ ̅ ̅ ̅ ̅
      A tool to help me with my trainee maintainer issue.

      Usage: trainee <command>

        trainee pull   # Update the draft with the relevant merge requests
        trainee edit   # Open the default editor to edit the draft
        trainee push   # Update the trainee maintainer issue with the draft content
        trainee open   # Open the trainee maintainer issue in the browser

    HELP
  end
end
