class Template
  SEPARATOR = "==============================================================================="
  TEMPLATE = <<~MARKDOWN
    ### %<title>s: %<url>s

    During review:
    - I suggested ...
    - I identified ...
    - I noted ...

    Post-review:
    - I missed ...
    - Merged as-is

    @%<merger>s Please add feedback, and compare this review to the average maintainer review.

    <hr>
    <sub>:robot: This message was generated with the help of [Trainee](https://gitlab.com/arturoherrero/trainee).</sub>
    %<separator>s
  MARKDOWN

  def initialize(merge_request)
    @merge_request = merge_request
  end

  def call
    format(
      TEMPLATE,
      title: merge_request.title,
      url: merge_request.web_url,
      merger: merge_request.merged_by.username,
      separator: SEPARATOR,
    )
  end

  private

  attr_reader :merge_request
end
